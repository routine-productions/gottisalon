<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="./css/index.min.css" type="text/css"/>
    <link rel="shortcut icon" href="/img/favicon.png" type="image/png">


</head>
<body>
<div class="wrapper">

    <?php
    require_once './modules/header.php';
    ?>
<main>
    <!--        Внутренняя страница-->
    <?php
    if ($_SERVER['REQUEST_URI'] == '/') {
        require_once './pages/contacts.php';
    } else {
        require_once './pages/' . $_SERVER['REQUEST_URI'] . '.php';
    }
    ?>
    <!--        Сайдбар сайдбар-->
    <?php
    require_once './modules/sidebar.php';
    ?>
</main>
    <?php
    require_once './modules/footer.php';
    ?>
</div>

<!--<script src="index.min.js"></script>-->
<!--<script src="jquery.actual.min.js"></script>-->


</body>
</html>

