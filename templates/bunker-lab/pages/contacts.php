<form class="Contacts-Form JS-Form"
        data-form-email='salongotti@mail.ru,olegblud@gmail.com'
        data-form-subject='Order form website'
        data-form-url='/js/data.form.php'
        data-form-method='POST'>
    <div>
        <input class="JS-Form-Require" type="text" placeholder="Ваше Имя:" name='name' data-input-title='Имя'>
        <input class="JS-Form-Require" type="text" placeholder="Ваш E-mail:" name='email' data-input-title='Email'>
        <input class="JS-Form-Require" type="text" placeholder="Тема:"  name='subject' data-input-title='Тема'>
    </div>

    <div>
        <textarea placeholder="Сообщение:" name="message" data-input-title='Сообщение'></textarea>
        <button class='JS-Form-Button'>Отправить</button>
    </div>

    <div class="JS-Form-Result"></div>
</form>