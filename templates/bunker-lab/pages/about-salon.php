<div class="Saloon">


    <h1>Уважаемые дамы и господа!</h1>

    <p>Салон красоты «Готти» рад приветствовать вас!</p>

    <p>Мы хотим, чтобы вместе с нами вы оставались молодыми, красивыми, жизнерадостными и здоровыми.</p>

    <p>Салон красоты «Готти» открылся в августе 2005 г. и расположился во вновь реконструированном здании делового
        центра Владимира по улице Большая Нижегородская. В основу дизайна положен готический стиль, отсюда и название
        салона – «Готти». Переплетение готики и современности, мягких пастельных и розовых тонов, создает гармоничную и
        расслабляющую атмосферу.</p>

    <p>Наш салон отвечает самым последним требованиям мировых стандартов индустрии красоты. Наши мастера -
        высококлассные, сертифицированные специалисты - работают на ультрасовременном оборудовании и используют самые
        передовые технологии.</p>

    <p>Мы предлагаем вам спектр услуг, необходимый для любого современного человека, который хочет быть здоровым,
        ухоженным и успешным.</p>

    <p>Мы уверены, что идеальная внешность не имеет возраста, а каждый клиент для нас – VIP.</p>

    <div class="sign_holder">Управляющий салоном Краковский М.В.

        <img class="Director-Sight" alt="image description" src="/img/signature.png"></div>

</div>