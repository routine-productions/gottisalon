<!--   Header-->
<header>
    <em class="slogan"> Красота есть<br>обещание счастья...</em>
    <strong class="logo"><a href="/"><span>ГОТТИ</span><em>САЛОН КРАСОТЫ</em></a></strong>
    <address>
        <a href="tel:+74922421045" class="tel">(4922)<span>42-10-45</span></a>
        <em>г. Владимир, ул. Большая Нижегородская, д.71</em>
    </address>

    <nav id="nav">
        <ul>
            <li><a href="/">О салоне</a></li>
            <li><a href="/services/">Услуги</a></li>
            <li><a href="/prices/">Цены</a></li>
        </ul>
        <ul>
            <li><a href="/photos/">Фотогалерея</a></li>
            <li><a href="/masters/">Мастера</a></li>
            <li><a href="/contacts/">Контакты</a></li>
        </ul>
    </nav>
</header>