<div class="sidebar">
    <div class="Sidebar-Cell">
        <h3>НАШИ УСЛУГИ</h3>
        <nav class="sidebar-nav">
            <ul>
                <jdoc:include type="modules" name="services"/>
            </ul>

        </nav>
    </div>
    <div class="Sidebar-Cell">
        <a href="vacancies">
            <h3>НАШИ ВАКАНСИИ</h3>
        </a>
        <div class="promo">
            <img src="/img/promo.jpg" alt=""/>
            <a href="/contacts/">
                <span class="text">Задать вопрос</span>
                <span class="link">ПОДРОБНЕЕ</span>
            </a>
        </div>

        <div class="news">
            <h3>НОВОСТИ</h3>
            <ul>
                <jdoc:include type="modules" name="news"/>
            </ul>
            <a href="/all-news/">все новости »</a>
        </div>

    </div>
    <div class="Sidebar-Cell">
        <h3>Мы Вконтакте:</h3>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
        <div id="vk_groups" style="height: 250px; width: 220px; background: none;">
            <iframe name="fXD65586" frameborder="0"
                    src="http://vk.com/widget_community.php?app=0&amp;width=220px&amp;_ver=1&amp;gid=63394320&amp;mode=0&amp;color1=FFFFFF&amp;color2=2B587A&amp;color3=5B7FA6&amp;class_name=&amp;height=250&amp;url=http%3A%2F%2Fgottisalon.ru%2Fphotos%2F&amp;referrer=http%3A%2F%2Fgottisalon.ru%2F&amp;title=%D0%A4%D0%BE%D1%82%D0%BE%D0%B3%D0%B0%D0%BB%D0%B5%D1%80%D0%B5%D1%8F%20%2F%20%D0%A1%D0%B0%D0%BB%D0%BE%D0%BD%20%D0%BA%D1%80%D0%B0%D1%81%D0%BE%D1%82%D1%8B%20%C2%AB%D0%93%D0%BE%D1%82%D1%82%D0%B8%C2%BB.%20%D0%92%D0%BB%D0%B0%D0%B4%D0%B8%D0%BC%D0%B8%D1%80.&amp;152b0ded5b2"
                    width="220" height="200" scrolling="no" id="vkwidget1"
                    style="overflow: hidden; height: 250px;"></iframe>
        </div>

    </div>


</div>