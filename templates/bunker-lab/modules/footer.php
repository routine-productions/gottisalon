<footer id="footer">
    <div class="footer-holder">
        <div class="footer-box">
            <em class="copyright"> © 2014 Салон красоты <a href="/">«Готти»</a></em>
            <span>Использование информации разрешается только при согласовании с владельцем.</span>
        </div>
        <em class="copyright"> © 2016  Создание сайта - <a href="http://progress-time.ru/">Progress Time</a></em>
    </div>
</footer>