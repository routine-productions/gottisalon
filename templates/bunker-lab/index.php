﻿<?php defined('_JEXEC') or die('Restricted access');
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
?>
<!DOCTYPE html>

<html lang="<?php echo $this->language; ?>">
<head lang="ru">
    <title><?= $doc->getTitle() ?></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./css/index.min.css" type="text/css"/>
    <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<div class="wrapper">

    <?php
    require_once __DIR__ . '/modules/header.php';
    ?>
    <main>
        <!--        Внутренняя страница-->
        <div class="Saloon">
            <jdoc:include type="modules" name="slider"/>

            <jdoc:include type="component"/>
            <?php
            if ($_SERVER['REQUEST_URI'] == '/contacts/') {
                require_once __DIR__ . '/pages/contacts.php';
            }
            ?>
        </div>

        <!--        Сайдбар сайдбар-->
        <?php
        require_once __DIR__ . '/modules/sidebar.php';
        ?>
    </main>
    <?php
    require_once __DIR__ . '/modules/footer.php';
    ?>
</div>


<script src="/index.min.js"></script>

</body>
</html>

