/*
 * Copyright (c) 2015
 * Routine JS - Modal
 * Version 0.3.0
 * Created 2015.12.03
 * Author Bunker Labs
 */
(function ($)
{


    $.fn.JS_Modal = function (Extended_Settings)
    {
        var Default_Settings = {
            Current: this,
            Modal: '.JS-Modal',
            Modal_Box: '.JS-Modal-Box',
            Modal_Close: '.JS-Modal-Close',
            Before_Show: function ()
            {
              $($('.Section-Slider-Item').get($(this.Target_Button).index()))
                  .addClass('Active').show().siblings().removeClass('Active').hide();
            },
            After_Show: function ()
            {
                if ($(window).width() < 1100 && $(this.Target).is('#Modal-100'))
                {
                    $('body,html').animate({'scrollTop': $(this.Target).position().top - $('.Site-Header .Top-Bar').outerHeight()});
                }
            },

            Before_Hide: function ()
            {
            },
            After_Hide: function ()
            {
            }
        };

        $.extend(this, Default_Settings, Extended_Settings);

        this.Show = function (Event)
        {
            this.Target_Button = Event.currentTarget;
            this.Target = $('#' + $(Event.currentTarget).attr('data-modal-id'));
            this.Duration = parseFloat(this.Target.css('transition-duration')) * 1000;

            this.Before_Show();
            this.Target.addClass('Visible').delay(this.Duration).promise().done(this.After_Show.bind(this));
        };

        this.Hide = function (Event)
        {
            this.Target = $(Event.currentTarget);
            if ($(Event.target).is(this.Modal))
            {
                this.Duration = parseFloat(this.Target.css('transition-duration')) * 1000;
                this.Target.removeClass('Visible')
                    .delay(this.Duration).promise().done(this.After_Hide);
                this.Before_Hide();
            } else if ($(Event.currentTarget).is(this.Modal_Close))
            {
                this.Target = this.Target.parents(this.Modal);
                this.Duration = parseFloat(this.Target.css('transition-duration')) * 1000;
                this.Before_Hide();

                this.Target.removeClass('Visible').delay(this.Duration).promise().done(this.After_Hide);
            }
            Event.stopPropagation();
        };

        this.click(this.Show.bind(this));

        $(this.Modal_Close + ',' + this.Modal).click(this.Hide.bind(this));

        return this;
    };


    $('.JS-Modal-Button').JS_Modal();
})(jQuery);

