/*
 * Copyright (c) 2015
 * Routine JS - Vertical Align
 * Version 0.1.0
 * Create 2015.12.02
 * Author Bunker Labs

 * Usage:
 *
 * Add class name 'JS-Vertical-Align' - to vertical align Element in Box
 * Add attribute  'data-shift'            - to set vertical shift
 * Add attribute  'data-min-width'        - to set min width for align
 * Add attribute  'data-max-width'        - to set max width for align

 * Code structure:
 * <div><img class="JS-Vertical-Align" data-min-width="768" data-max-width="1000" src="..." alt="..."></div>
 */
(function ($) {
    $(document).ready(function () {
        Vertical_Align();
        $(window).resize(function () {
            Vertical_Align();
        });

        function Vertical_Align() {
            $('.JS-Vertical-Align').each(function () {
                var Element_Shift = $(this).attr('data-shift') ? $(this).attr('data-shift') : 0,
                    Min_Width     = $(this).attr('data-min-width') ? $(this).attr('data-min-width') : 0,
                    Max_Width     = $(this).attr('data-max-width') ? $(this).attr('data-max-width') : Infinity;

                if ($(window).width() >= Min_Width && $(window).width() <= Max_Width) {
                    $(this).css('margin-top',
                        ($(this).parent().actual('outerHeight') / 2) - ($(this).actual('outerHeight') / 2) + Element_Shift
                    );
                } else {
                    $(this).css('margin-top', '');
                }
            });
        }
    });
})(jQuery);
