/*
 * Copyright (c) 2015
 * Routine JS - Form
 * Version 0.1.1
 * Create 2015.12.03
 * Author Bunker Labs


 <form class='JS-Form'
 data-form-email='onevanivan1@gmail.com'
 data-form-subject='Order form website'
 data-form-url='/public/js/data.form.php'
 data-form-method='POST'>
 <input type='text' name='name' data-input-title='Your first name'>
 <input type='text' name='password' data-input-title='Your password'>
 <input type='radio' name='thisname' data-input-title='Your check'>

<button name="sex" data-input-title='Your sex'>
 <input class='JS-Form-Numeric' type='text' name='phone' data-input-title='Your telephone number'>
 <input class='JS-Form-Email' type='text' name='email' data-input-title='Your e-mail'>
 <textarea name='message' data-input-title='Your e-mail'></textarea>
 <button class='JS-Form-Button'></button>
 <div class="JS-Form-Result"></div>
 </form>



 */
(function ($) {
    $(document).ready(function () {

        var $Button = $('.JS-Form-Button'),
            Forms = '.JS-Form',
            $Result = $('.JS-Form-Result'),
            Input = 'textarea,input,select',
            Validate_Numeric = $('.JS-Form-Numeric'),
            Validate_Email = $('.JS-Form-Email'),
            Message_Error = 'Заполните все поля!';


        $Button.click(function () {
            var Data = {},
                Form = $(this).parents(Forms),
                Url = Form.attr('data-form-url'),
                Method = Form.attr('data-form-method'),
                Valid = true;

            // Validate
            var Numeric = $(Validate_Numeric, Form).val(),
                Email_Exp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                Email = $(Validate_Email, Form).val();

            if (Numeric && !$.isNumeric(Numeric)) {
                Valid = Message_Error;
            } else if (Email && !Email_Exp.test(Email)) {
                Valid = Message_Error;
            }

            // Collect Data
            Data.input = {};
            Data.email = Form.attr('data-form-email');
            Data.subject = Form.attr('data-form-subject');


            $(Input, Form).each(function () {

                var Title = $(this).attr('data-input-title'),
                    Value = $(this).val(),
                    Name_Attr = $(this).attr('name'),
                    Data_Type = $(this).attr('type');


                if ((Title && Value && Data_Type == 'text') || ((Data_Type == 'radio' || Data_Type == 'checkbox' ) && $(this).is(':checked') && Title && Value)) {
                    Data.input[Name_Attr] = {};
                    Data.input[Name_Attr].title = Title;
                    Data.input[Name_Attr].value = Value;
                }

                if($(this).hasClass('JS-Form-Require') && (!Title || !Value)){
                    Valid = Message_Error;
                    $(this).addClass('JS-Message-Error');
                }

            });


            // Send Form
            if (Valid == true) {
                $.ajax({
                    url: Form.attr('data-form-url') ? Form.attr('data-form-url') : 'data.form.php',
                    type: Form.attr('data-form-method') ? Form.attr('data-form-method') : 'POST',
                    data: Data,

                    success: function (Data) {
                        Form.find($Result).text(Data);
                        $('input[type=text],textarea', Form).val('');
                    },
                    error: function (Data) {
                        Form.find($Result).text(Data);

                    }
                });
            } else {
                Form.find($Result).text(Valid);
            }

            return false;
        });

        $(Input).click(function () {
            $(this).removeClass('JS-Message-Error');
        });
    });
})(jQuery);

