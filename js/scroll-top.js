/*
 * Copyright (c) 2015
 * Routine JS - Scroll Top
 * Version 0.1.1
 * Create 2015.12.22
 * Author Bunker Labs

 * Usage:
 *1)add class "JS-Scroll-Top-Button" to button
 *2)add attribute 'data-scroll-duration' in the seconds [0;infinity]

 * Code structure:
 *   <div data-scroll-duration='3000' class="JS-Scroll-Top">
 *      <span>Scroll Top</span>
 *   </div>
 *
 *

 */
(function ($) {
    $(document).ready(function () {

        $('.JS-Scroll-Top').click(function(){

            var Scroll_Duration_String = $(this).attr('data-scroll-duration')?$(this).attr('data-scroll-duration'):800,
                Scroll_Duration = parseInt(Scroll_Duration_String);

            $('body, html').animate({

                scrollTop:0
            }, Scroll_Duration );

            return false;
        });
    });
})(jQuery);
