/*
 * * Code structure:
 *  <div class="Script-Menu">
 *       <div class="Menu-Button" data-height-menu="6">
 *          <span>Click Here</span>
 *       </div>
 *       <div class="Menu-List">
 *           <ul>
 *               <li>Item 1</li>
 *               <li>Item 2</li>
 *               <li>Item 3</li>
 *               <li>Item 4</li>
 *               <li>Item 5</li>
 *               <li>Item 6</li>
 *               <li>Item 7</li>
 *               <li>Item 8</li>
 *               <li>Item 9</li>
 *               <li>Item 10</li>
 *           </ul>
 *       </div>
 *   </div>
 * */


(function ($)
{

    $.fn.DropDownMenu = function ()
    {

        var $Global_This = $(this);


        $Global_This.find('.Script-Menu .Menu-Button').click(function ()
        {


            var $Button = $(this);

            if ($Button.parents('.Script-Menu').find('.Menu-List').is(':hidden'))
            {

                var Actual_Height = $Button.parents('.Script-Menu').find('.Menu-List > *').actual('outerHeight');

                $Global_This.find('.Script-Menu .Menu-List').css({'display': 'none', 'height': '0'});
                $(this).parents('.Script-Menu').find('.Menu-List').css({'display': 'block', 'height': Actual_Height});


            } else
            {
                function Time_Display()
                {
                    $Button.parents('.Script-Menu').find('.Menu-List').css({'display': 'none'});
                }

                $Button.parents('.Script-Menu').find('.Menu-List').css({'height': '0px'});
                setTimeout(Time_Display, 200);
            }

            return false;
        });



    };

    $(".Prices").DropDownMenu();


})(jQuery);