<?php
//print_r($items);
//exit;
//?>

<div class="Saloon-Slider">
    <ul class="Saloon-Slide">
        <?php foreach ($items as $key => $item): ?>
            <?php foreach ($item->attachments as $key_attachment => $attachment): ?>
                <?php if ($key_attachment == 0) { ?>
                    <li class="Section-Slider-Item Active" style="background-image: url(<?= $attachment->link; ?>)"></li>
                <?php } else { ?>
                    <li class="Section-Slider-Item" style="background-image: url(<?= $attachment->link; ?>)"></li>
                <?php } ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </ul>


    <ul class="Slider-Nav">
        <?php foreach ($items as $key => $item): ?>
            <?php foreach ($item->attachments as $key_attachment => $attachment): ?>
                <?php if ($key_attachment == 0) { ?>
                    <li class="Section-Slider-Button Active"></li>
                <?php } else { ?>
                    <li class="Section-Slider-Button"></li>
                <?php } ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </ul>
    <ul class="flex-direction-nav">
        <li class="flex-nav-prev Section-Slider-Previous">
            <svg class="flex-prev">
                <use xlink:href="/img/arrow.svg#arrow"></use>
            </svg>
        </li>
        <li class="flex-nav-next Section-Slider-Next">
            <svg class="flex-next">
                <use xlink:href="/img/arrow.svg#arrow"></use>
            </svg>
        </li>
    </ul>
</div>