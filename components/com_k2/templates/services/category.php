<div class="Services">
    <?php foreach ($this->leading as $key => $item): ?>
        <div class="Service-Content">
            <div class="Service-Top" style="background-image: url(<?="/media/k2/items/src/".md5("Image".$item->id).".jpg"?>);">

            </div>
            <div class="Service-Bottom">
                <h4><?= $item->title ?></h4>

                <?= $item->introtext ?>
            </div>

            <a href="<?= $item->link ?>">Подробнее</a>
        </div>
    <?php endforeach; ?>
</div>