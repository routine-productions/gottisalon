<div class="Photogallery">
    <?php foreach ($this->item->attachments as $attachment): ?>
        <div class="Photo-Wrap JS-Modal-Button" data-modal-id="Modal-Gallery">
            <div class="Photo" style="background-image:url(<?= $attachment->link ?>);"></div>
        </div>
    <?php endforeach; ?>
</div>


<div class="JS-Modal JS-Carousel flex-direction-nav" id="Modal-Gallery">
    <div class="JS-Modal-Box">
        <div class="JS-Modal-Close"></div>
        <div class="Section-Slider-Previous flex-nav-prev">
            <svg class="flex-prev">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/arrow.svg#arrow"></use>
            </svg>
        </div>
        <div class="Section-Slider-Next flex-nav-next">
            <svg class="flex-next">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/arrow.svg#arrow"></use>
            </svg>
        </div>

        <div class="Section-Slider-List">
            <?php foreach ($this->item->attachments as $key => $attachment): ?>
                <div class="Section-Slider-Item <?=($key==0)?' Active':'' ?>" style="background-image:url(<?= $attachment->link ?>);"></div>
            <?php endforeach; ?>
        </div>
    </div>
</div>