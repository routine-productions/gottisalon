<div class="Masters">
    <?php foreach ($this->leading as $key => $item): ?>

        <div class="Master-Wrap">
            <img src="<?="/media/k2/items/src/".md5("Image".$item->id).".jpg"?>" alt=""/>
            <div>
                <h4><?= $item->title ?></h4>
                <p><strong><em><?= $item->extra_fields[0]->value ?></em></strong></p>

                <?= $item->introtext ?>
                <?= $item->fulltext ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>