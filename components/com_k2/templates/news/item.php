<div class="New">
	<h1>Новости:</h1>

	<div class="New">
		<h4><?= $this->item->title ?> <span class="News-Date">(<?= JHTML::_('date', $this->item->created, 'd F Y'); ?>)</span></h4>

		<?= $this->item->introtext ?>
		<?= $this->item->fulltext ?>

		<a href="#" onclick="window.history.back(1);return false;">« вернуться</a>
	</div>
</div>