<div class="content">
    <h1>Новости:</h1>

    <div class="sidebar2">
        <ul class="news">
            <?php foreach ($this->leading as $key => $item): ?>
                <li><span class="date"><?= JHTML::_('date', $item->created, 'd F Y'); ?></span><a
                        href="<?= $item->link ?>" class="title"><?= $item->title ?></a></li>
            <?php endforeach; ?>

            <?php if ($this->pagination->getPagesLinks()): ?>
                <?php if ($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>
            <?php endif; ?>
        </ul>
    </div>
</div>