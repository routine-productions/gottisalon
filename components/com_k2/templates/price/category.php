<div class="Prices">
    <h1>Прейскурант на услуги:</h1>
    <?php foreach ($this->leading as $key => $item): ?>

        <div class="Price-List Script-Menu">
            <div class="Menu-Button">
                <h4><?= $item->title ?></h4>
                <svg>
                    <use xlink:href="/img/arrow.svg#arrow"></use>
                </svg>
            </div>
            <div class="Menu-List">
                <article>
                    <?= $item->introtext ?>
                </article>

            </div>
        </div>
    <?php endforeach; ?>
</div>